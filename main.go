package main

import (
	"context"
	pb "bitbucket.org/jharshman/grpc-demo/pkg/grpc"
	"google.golang.org/grpc"
	"net"
)

type handler struct{}

func (h *handler) Fetch(ctx context.Context, empty *pb.Empty) (*pb.Gophers, error) {
	return &pb.Gophers{}, nil
}

func main() {
	// init gRPC server
	lis, err := net.Listen("tcp", ":8080")
	if err != nil {
		panic(err)
	}
	defer lis.Close()

	grpcServer := grpc.NewServer()
	handler := &handler{}
	pb.RegisterGopherServer(grpcServer, handler)
	grpcServer.Serve(lis)
}
