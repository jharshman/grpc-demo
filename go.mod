module bitbucket.org/jharshman/grpc-demo

go 1.12

require (
	github.com/golang/protobuf v1.3.3
	google.golang.org/grpc v1.27.1
)
