.PHONY: protobuf

protobuf:
	protoc --proto_path=pkg/grpc --go_out=plugins=grpc:pkg/grpc demoserver.proto
